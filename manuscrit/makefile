SOURCES= manuscrit.tex
CHAPITRES= Arbres_Non_Ambigus/arbres_non_ambigus.tex Generalites/generalites.tex commandes_couleurs.tex ASEP/asep.tex Introduction/introduction.tex notations.tex BXthesis.tex Perspectives/perspectives.tex PPP/ppp.tex
IMAGES= \
	images/ana_geo_ex_contre_ex.tex images/ana_equivalence_defs.tex \
	images/ana_anag_anad.tex images/ana_zigzag.tex images/ana_equerres.tex \
	images/ana_zigzag_cycle.tex \
	images/ana_preuve_decomposition_arbre_bicolore_bidecroissant_1_4.tex \
	images/ana_preuve_decomposition_arbre_bicolore_bidecroissant_2_4.tex \
	images/ana_preuve_decomposition_arbre_bicolore_bidecroissant_3_4.tex \
	images/ana_preuve_decomposition_arbre_bicolore_bidecroissant_4_4.tex \
	images/ana_bijABAO.tex images/ana_geo_aretes.tex images/ana_ab_fixe.tex \
	images/ana_Bxy_ab.tex images/ana_ab_alpha_beta.tex \
	images/ana_ab_parcours.tex images/ana_parcours_ana.tex \
	images/ana_arbre_bicolore_bidecroissant.tex \
	images/ana_anag_a.tex images/ana_anag_sous_arbres.tex \
	images/ana_Mx_anag.tex images/gen_arbres_ordonnes.tex \
	images/gen_m_aire.tex images/gen_arbre_ordonne_m_aire.tex \
	images/asep_operations_ele.tex images/asep_te_insertions_ex.tex \
	images/asep_te_insertions_schema.tex images/asep_te.tex \
	images/asep_te_q.tex images/asep_m_ferrer.tex \
	images/asep_correspondance_I_te.tex images/asep_inv_gd.tex \
	images/asep_inv_if.tex images/asep_inv_ph.tex images/asep_regles_q_u.tex \
	images/asep_inv_ph_fail.tex images/asep_regles_q_u_alternatives.tex \
	images/asep_te_B.tex images/asep_te_B_demi.tex images/asep_TE_in_TEB.tex \
	images/asep_operation_ele_sym.tex \
	images/asep_te_insertions_sym_schema.tex \
	images/asep_te_insertions_sym_ex.tex images/gen_markov_deux_etats.tex \
	images/gen_markov_reductible.tex images/asep_probabilites.tex \
	images/asep_tb.tex images/gen_ferrers_4.tex \
	images/gen_ferrers_bord_sudest.tex images/asep_coins_occupes.tex \
	images/gen_ferrers_diag_principale.tex images/asep_cna_co.tex \
	images/asep_cna_co_preuve.tex images/asep_tb_to_PASEP.tex \
	images/gen_ferrers_decale.tex images/asep_tp.tex images/asep_ta.tex \
	images/asep_ferrers_tp_ta_tb.tex images/asep_ferrers_tp_ta_tb_type_B.tex \
	images/asep_cv_ab.tex images/asep_cv_ab_bij.tex \
	images/asep_cv_a1_1b_bij.tex images/asep_coin_triplet_tb_tb_ana.tex \
	images/asep_cnas_co_preuve.tex images/asep_deux_diagramme_rhombique.tex \
	images/asep_deux_tuiles.tex images/asep_deux_diagramme_rhombique_pave.tex \
	images/asep_deux_ter.tex images/asep_deux_regles_q_u.tex \
	images/asep_deux_preuve_Z_aaaa_pair.tex \
	images/asep_deux_insertions_ex.tex \
	images/asep_deux_operations_ex.tex images/asep_te_nobl.tex \
	images/asep_deux_probabilites.tex images/asep_deux_regles_q_u_ad.tex \
	images/gen_graph.tex images/gen_arbre_term.tex \
	images/gen_ab_decroissant.tex images/asep_te_cellules_spe.tex \
	images/gen_pp.tex images/PPP_pp_a.tex images/gen_pp_perm321.tex \
	images/gen_chemin_6.tex images/gen_chemin_inf.tex images/PPP.tex \
	images/PPP_degenere.tex images/PPP_non_degenere.tex \
	images/PPP_coupe_chemins.tex images/PPP_marque.tex images/PPP_bandes.tex \
	images/PPP_marque_perm_affine_321.tex images/PPP_struct_arbo.tex \
	images/PPP_suppression_colonne.tex images/PPP_min2.tex images/PPP_min.tex \
	images/gen_chemin_Dyck.tex images/gen_chemin_Dyck_arbre_ordonne.tex \
	images/PPP_chemin_Dyck_aire_triangulaire.tex images/PPP_suppr.tex \
	images/PPP_rotation.tex images/PPP_maigre.tex images/PPP_maigre_psi.tex \
	images/PPP_cellules_admissibles.tex images/PPP_arbre_ordonne_aire_triangulaire.tex \
	images/PPP_carac_intr_mince_numerotation.tex images/PPP_intr_mince.tex \
	images/PPP_chemin_Dyck_aire_triangulaire_mult.tex images/gen_graphe_planaire.tex \
	images/gen_carte_deformation.tex images/PPP_hi_1_carte_3.tex

################################################################################
ALL_SOURCES=$(SOURCES) $(IMAGES)
LOG_FILES=$(ALL_SOURCES:.tex=.log)
OUT_FILES=$(ALL_SOURCES:.tex=.out)
SNM_FILES=$(ALL_SOURCES:.tex=.snm)
TOC_FILES=$(ALL_SOURCES:.tex=.toc)
NAV_FILES=$(ALL_SOURCES:.tex=.nav)
AUX_FILES=$(ALL_SOURCES:.tex=.aux)
PDF_FILES=$(ALL_SOURCES:.tex=.pdf)
BBL_FILES=$(ALL_SOURCES:.tex=.bbl)
BLG_FILES=$(ALL_SOURCES:.tex=.blg)

PDF_SOURCES=$(SOURCES:.tex=.pdf)
PDF_IMAGES=$(IMAGES:.tex=.pdf)

.PHONY: all default clean images

default: $(PDF_SOURCES)

all: $(PDF_IMAGES)
	for f in $(SOURCES); do \
		pdflatex  -output-directory $$(dirname $$f) $$f; \
		bibtex $$(basename $$f .tex); \
		pdflatex  -output-directory $$(dirname $$f) $$f; \
		makeindex -s index.ist $$(basename $$f .tex).idx; \
		pdflatex  -output-directory $$(dirname $$f) $$f; \
	done

images/%.pdf:images/%.tex
	pdflatex -output-directory $$(dirname $<) $?

%.pdf:%.tex $(PDF_IMAGES) $(CHAPITRES)
	pdflatex -output-directory $$(dirname $<) $<

images: make_images default

make_images:
	for f in $(IMAGES); do \
		pdflatex  -output-directory $$(dirname $$f) $$f; \
	done

clean:
	-rm -f $(LOG_FILES)
	-rm -f $(OUT_FILES)
	-rm -f $(SNM_FILES)
	-rm -f $(TOC_FILES)
	-rm -f $(NAV_FILES)
	-rm -f $(AUX_FILES)
	-rm -f $(BBL_FILES)
	-rm -f $(BLG_FILES)
#	-rm -f $(PDF_FILES)
