\subsection{Cas particulier où $\gamma=\delta=0$}
Si on fixe $\gamma=\delta=0$, le modèle ASEP devient le modèle PASEP (Partially
asymetric exclusion process). L'ansatz matriciel devient alors,
\begin{equation}\label{eq:asep_sys_ansatz_PASEP}
    \hspace{1cm}
    \left \{
        \begin{array}{lll}
            \pte(X\!\no\!\bl Y)
            &= q\pte(X\!\bl\!\no Y)
            &+ \alpha\beta\left(\pte(X\!\no Y)+\pte(X\!\bl Y)\right)
            \\
            \pte(\bl\! X)
            &= \beta \pte(X)
            &
            \\
            \pte(X\no)
            &= \alpha \pte(X)
            &
            \\
            \pte(\epsilon)
            &= 1
            &
            \\
        \end{array}
    \right.
    ,
\end{equation}
Corteel et Williams ont montré dans \cite{CWtp1} que les tableaux de permutation
permettent de donner une interprétation combinatoire aux probabilités de l'état
stationnaire. En effet, pour un mot $m$,
$\pte(m)$ s'interprète comme une série génératrice de tableaux de permutations.
Pour cela, les auteurs ont construit des matrices de dimension infinie $D$ et
$E$, ainsi que deux vecteurs $W$ et $V$ vérifiant le système en termes
matriciels équivalent au Système~\eqref{eq:asep_sys_ansatz_PASEP}, puis en
démontrant que les entrées d'un produit quelconque $M$ des matrices $D$ et $E$,
s'interprète en terme de série génératrice. Dans un autre papier (\cite{CWtp2}),
les auteurs définissent une chaine de Markov sur les tableaux de permutations qui
se projette, au sens de \cite[Section~4.5]{CWtp2}, sur le modèle PASEP. Donnant
ainsi, une interprétation combinatoire plus forte du modèle PASEP en terme de
tableau de permutation. Les tableaux de permutation sont en bijection avec les
\emph{tableaux alternatifs} introduit par Viennot (\cite{V}). Ces tableaux
apparaissent naturellement comme interprétation combinatoire de l'état
stationnaire du PASEP, en utilisant la théorie de l'\emph{ansatz cellulaire}
développé par Viennot \cite{}. Nous appliquons cette théorie dans la
Section~\ref{}, pour montrer que les \emph{tableaux boisés}, qui correspondent
essentiellement aux tableaux alternatifs, donnent une interprétation
combinatoire du Système~\eqref{eq:asep_sys_ansatz_PASEP}.
\todo{chapeau}
\subsubsection{Ansatz cellulaire}
Nous présentons ici sans donner de preuves, l'ansatz cellulaire introduit par
Viennot \cite{??}. Dans cette section, $E$ et $J$ désignent deux ensembles
quelconques. Soit $\K$ un corps, on note $\mathcal A$ l'algèbre des polynômes
$\K\left[\{X_e,e\in E\}\cup\{Y_i,i\in J\}\right]$. On note $\mathcal
X=\{X_e,e\in E\}$ et $\mathcal Y=\{Y_j,j\in J\}$. Soit
\(
    c=(c_{e,i}^{f,j})_{(e,i,f,j)\in E\times J\times E\times J}
\)
un famille d'éléments de $\mathbb K$, on note $I_c$ l'idéal bilatère engendré
par les polynômes
\begin{equation}\label{eq:asep_quad}
    Y_iX_e-\sum_{(f,j)\in E\times J}c_{e,i}^{f,j}X_fY_j.
\end{equation}
On note $\mathcal Q$ le quotient de $\mathcal A$ par $I$, et pour tout élément
$m\in\mathcal A$, on note $\overline{m}$ sa projection dans $\mathcal Q$. On dit
que $Q$ est une algèbre quadratique car l'idéal $I_c$ est engendré par des
polynômes homogènes de degré deux. En analogie aux mots, on note $\mathcal Z^*$
l'ensemble des monômes de $\mathcal A$ dont les indéterminés sont dans $\mathcal
Z$.
\begin{lemme}
    L'ensemble $\mathcal B=\{\overline{uv},(u,v)\in\mathcal X^*\times\mathcal
    Y^*\}$ est une base de l'algèbre $\mathcal Q$ en tant que $\K$-espace
    vectoriel.
\end{lemme}
En d'autres termes, pour tout $m\in(\mathcal X\cup\mathcal Y)^*$ il existe une
unique façon d'écrire
\[
    \overline{m}
    =
    \sum_{(u,v)\in\mathcal X^*\times\mathcal Y^*}c(u,v;m)\overline{uv},
\]
où $c(u,v;m)\in\mathbb K$. En particulier, le calcul de la décomposition de
$\overline{m}$ ne dépend pas de l'ordre dans lequel on utilise les
Équations~\eqref{eq:asep_quad}. L'ansatz cellulaire consiste à encoder ce calcul
dans des tableaux.
\begin{definition}
    Un \emph{$\mathcal Q$-tableau complet} est un diagramme de Ferrers dont les
    cellules étiqueté de la façon suivante
    \begin{center}
        \begin{tikzpicture}[scale=.6,every node/.style={transform shape}]
            \draw (0,0) --++ (2,0) --++ (0,2) --++ (-2,0) --++ (0,-2);
            \node at (1.7,1) {\LARGE$i$};
            \node at (.3,1) {\LARGE$j$};
            \node at (1,.3) {\LARGE$e$};
            \node at (1,1.6) {\LARGE$f$};
        \end{tikzpicture}
    \end{center}
    où $(e,i,f,j)\in E\times J\times E\times J$, avec la condition que les deux
    indices des deux côtés d'une arête sont les mêmes.
\end{definition}

Pour cela, à tout monôme $m=m_1\cdots m_n$, où
$m_i\in\in\mathcal X\cup\mathcal Y$ pour tout $i\in\llbracket 1,n \rrbracket$,
on lui associe le diagramme de Ferrers dont le bord sud-est est égal à
$b_1\cdots b_n$, où $b_i$ est un pas vertical si $m_i\in\mathcal Y$ et un pas
horizontal si $m_i\in\mathcal X$. La Figure~\ref{fig:asep_m_ferrer} illustre
cette correspondance.
\begin{figure}
    \begin{center}
        \includegraphics[scale=.8]{images/asep_m_ferrer}
        \caption{Le diagramme de Ferrer associé au mot
            $Y_{i_1}X_{e_1}Y_{i_2}Y_{i_3}X_{e_2}Y_{i_4}X_{e_3}Y_{i_5}$}
        \label{fig:asep_m_ferrer}
    \end{center}
\end{figure}
\todo{finir le ansatz matriciel}


\subsubsection{Interprétation combinatoire du PASEP par les tableaux boisés}

Explication du matrix ansatz cellular, et obtention des tableaux boisés par ce procédé.
Rappels de propriétés vérifiées par les TLT que l'on utilisera pour les coins.

\subsubsection{Type B}

\subsection{Cas général}
\subsubsection{$q=1$, Tableaux boisés étiquetés.}
Explication de pourquoi le matrix ansatz ne marche pas.
On peut contourner le problème en mettant $q=1$,
on obtient les tableaux boisés étiquetés.
