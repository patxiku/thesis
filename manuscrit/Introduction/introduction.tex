\stoptocwriting
Le travail de recherche effectué au cours de cette thèse relève de la
combinatoire énumérative. Trois problèmes ont été abordés. D'une part, nous
proposons une nouvelle approche pour étudier l'interprétation combinatoire, par
les tableaux, de l'état stationnaire de l'ASEP débutée par Corteel et Williams.
D'autre part, nous étudions les structures arborescentes sous-jacentes à cette
interprétation, en les généralisant notament à toutes les dimensions. Enfin,
nous donnons une interprétation des polyominos parallélogrammes périodiques par
des structures arborescentes.

\section*{Contexte du travail}
Nous commençons par décrire sommairement la combinatoire énumérative, puis nous
présentons le contexte physique dans lequel se placent les deux premières
parties de ce manuscrit. Enfin, nous présentons l'outil informatique qui nous a
assisté durant notre travail de recherche. Les termes mathématiques non définis
dans cette introduction le seront par la suite.

\subsection*{Combinatoire énumérative}
La \emph{combinatoire}\index{combinatoire} est la science des structures
discrètes et dénombrables. De ce fait, on rencontre des problèmes combinatoires
dans de nombreux contextes mathématiques : l'algèbre, la géométrie, la théorie
des probabilités, la topologie\dots mais aussi dans d'autres sciences :
l'informatique, la physique statistique et la biologie.

La \emph{combinatoire énumérative} \index{combinatoire!énumérative} étudie plus
particulièrement l'énumération, à taille fixée, de ces structures.
Une \emph{classe combinatoire}\index{combinatoire!classe} est un ensemble
$\C$ muni d'une application \emph{taille}
\index{combinatoire!classe!taille} $\taille:\C\rightarrow \N$ telle que
pour tout $n\in\N$ l'ensemble $\C_n:=\{C\in\C,\;\taille(C)=n\}$
est fini. Un élément $C$ d'une classe combinatoire $\C$ est appelé un
\emph{objet (combinatoire)} \index{combinatoire!objet} et la \emph{taille}
\index{combinatoire!objet!taille} de $C$ correspond à $\taille(C)$. Ainsi, on
cherche à trouver une formule satisfaisante pour le nombre d'éléments de $\C_n$.
La réponse est parfois facile à donner, par exemple le nombre de mots de
$n$ lettres que l'on peut construire avec un alphabet à deux éléments est $2^n$.
Ici, c'est le nombre de lettres d'un mot qui joue le rôle de la taille. Une
autre façon d'y apporter une solution est de mettre en bijection la classe
combinatoire que l'on veut énumérer avec une classe combinatoire que l'on sait
énumérer. Par exemple, il existe de nombreuses classes combinatoires comptées
par les \emph{nombres de Catalan} \index{nombre!de Catalan}
\[
    Cat_n=\frac{1}{n+1}\binom{2n}{n}.
\]
Le nombre d'arbres binaires à $n$ sommets, d'arbres ordonnés à $n+1$ sommets, de
chemins de Dyck de longueur $2n$ et de polyominos parallélogrammes de
demi-périmètre $n+1$, pour ne citer qu'eux, est égal à $Cat_n$. On peut montrer
par le lemme cyclique (ou lemme de Raney) \cite{Lot} que les chemins de Dyck de
longueur $2n$ sont comptés par Catalan. Étant donné qu'il existe des bijections
entre ces différentes classes combinatoires, elles le sont toutes. Enfin,
certaines classes combinatoires peuvent être décomposées récursivement. Par
exemple si on veut compter le nombre $a_n$ de façon d'ordonner totalement $n$
éléments $e_1,\ldots,e_n$, il suffit d'abord d'ordonner $e_1,\ldots,e_{n-1}$
puis de choisir l'emplacement de $e_n$ parmi les $n$ emplacements disponibles.
De ce fait, la suite $(a_n)_{n\geqslant 1}$ vérifie la récurrence
\[
    \left\{
        \begin{array}{lll}
            a_n &=n\,a_{n-1} &\text{ si }n\geqslant 2,\\
            a_1 &=1,
        \end{array}
    \right.
\]
ce qui nous permet d'en déduire que $a_n=n!$. Il n'est pas toujours possible de
trouver une formule directement à partir de la relation de récurrence. Il est
donc souvent utile d'associer à une classe combinatoire $\C$ la série
formelle
\[
    \FGC(x):=\sum_{n\geqslant 0}|\C_n| x^n
\]
appelée \emph{série génératrice (ordinaire)}.
\index{série génératrice!ordinaire} On peut ainsi convertir la récurrence
vérifiée par la suite $(|\C_n|)_{n\geqslant 0}$ en une équation vérifiée par
$\FGC(x)$. Si bien que nous avons de cette manière accès à l'outillage
mathématique permettant d'étudier des équations fonctionnelles. Pour
certaines classes combinatoires, notamment les classes combinatoires dites
étiquetées, il peut être intéressant de considérer plutôt la
\emph{série génératrice exponentielle} \index{série génératrice!exponentielle}
\[
    \FGC(x):=\sum_{n\geqslant 0}|\C_n| \frac{x^n}{n!}.
\]

Outre la taille, l'étude d'une classe combinatoire nous amène régulièrement à
considérer d'autres fonctions de $\C$ dans $\N$ que nous appelons
\emph{statistiques}.\index{statistique} Soit
$\stat_1,\ldots,\stat_k:\C\rightarrow \N$ des statistiques sur une classe
combinatoire $\C$. Naturellement, le problème se pose de raffiner les résultats
précédents en prenant en compte ces statistiques. Par exemple, est-il possible
de trouver une formule, à taille fixée, pour le polynôme générateur
\[
    \FGC_n(x_1,\ldots,x_k)
    :=
    \sum_{C\in\C_n}x_1^{\stat_1(C)}\cdots x_k^{\stat_k(C)}\text{ ?}
\]
Ou encore, soit $\C'$ une classe combinatoire en bijection avec $\C$ et $f$ une
bijection correspondante. Existe-t-il des statistiques naturelles
$\stat'_1,\ldots,\stat'_k:\C'\rightarrow \N$ sur $\C'$ telles que pour tout
élément $C$ de $\C_n$ on ait
\[
    (\stat_1(C),\ldots,\stat_k(C))
    =
    (\stat'_1(f(C)),\ldots,\stat'_k(f(C))) ?
\]
Réciproquement, supposons que nous ayons montré par le calcul que l'on a
\[
    \FGC_n(x_1,\ldots,x_k)
    =
    \FGC'_n(x_1,\ldots,x_k).
\]
C'est-à-dire que les statistiques $(\stat_1,\ldots,\stat_k)$ et
$(\stat'_1,\ldots,\stat'_k)$ sont \emph{équidistribuées}.
\index{equi@équidistribué} Une preuve combinatoire de ce résultat consiste à
définir une bijection $f$ de $\C$ dans $\C'$ telle que
\[
    (\stat_1(C),\ldots,\stat_k(C))
    =
    (\stat'_1(f(C)),\ldots,\stat'_k(f(C))).
\]

Un autre aspect de ce domaine est de donner une interprétation combinatoire
d'une suite d'entiers, d'un polynôme à coefficients positifs ou d'une série
formelle apparaissant dans d'autres contextes. Par exemple, dans le cas d'un
polynôme $P$ il s'agit de définir une classe combinatoire $\C$ munie de
statistiques de sorte que
\[
    P=\FGC_n(x_1,\ldots,x_k)
\]
pour un certain $n$. Porter un regard combinatoire sur des problèmes externes
spécifiques est parfois révélateur de liens plus profonds. La physique
statistique, et plus précisément le processus d'exclusion simple, en fournit un
exemple remarquable.

\subsection*{Processus d'exclusion simple}
Le processus d'exclusion simple (SEP) à une dimension est un modèle de physique
statistique introduit de manière indépendante en biologie par MacDonald, Gibbs
et Pipkin \cite{MGP} ainsi qu'en mathématiques par Spitzer \cite{ASEPmath} dans les
années 70. C'est un système de particules en interaction qui peuvent sauter d'un
pas vers la droite ou vers la gauche sur un réseau à une dimension ; chaque
emplacement du réseau ne peut contenir qu'une particule au maximum. Le SEP est
devenu un modèle central de la physique statistique hors équilibre. D'une part,
il est simple ce qui permet l'obtention de résultats exacts. D'autre part, il
présente des propriétés macroscopiques non triviales que l'on retrouve de
manière générique dans les systèmes hors équilibre plus réalistes : transitions
de phase \cite{DEHP,USW}, chocs \cite{ABL,DJLS}, corrélations à longue distance
\cite{DE,DJS}. Depuis son introduction, il a fait l'objet de nombreuses
applications dans des domaines variés. On peut par exemple citer l'étude de la
cinétique de la biopolymérisation \cite{MGP}, la diffusion de particules à
travers un milieu poreux ou des canaux microscopiques \cite{CL}, la dynamique
d'un polymère en milieu dense \cite{Sc} et l'étude du trafic routier
\cite{SWtrafic}. Il a également des connexions avec les polynômes orthogonaux
\cite{Sasa,USW} et le modèle XXZ \cite{ER,SS}.

Le SEP est défini comme une chaine de Markov à temps continu ou à temps
discret. Ces deux versions sont équivalentes dans le sens où la probabilité
stationnaire est la même dans les deux cas. De même que dans la littérature
combinatoire nous faisons le choix de la version discrète du SEP.

Notons $q$ et $u$ les probabilités qu'une particule saute respectivement vers
la gauche et vers la droite. Le SEP prend des noms différents suivant les
valeurs de $q$ et $u$ :
\begin{itemize}
    \item le processus d'exclusion simple symétrique (SSEP) : les probabilités
        $q$ et $u$ sont égales ;
    \item le processus d'exclusion simple asymétrique (ASEP) : les probabilités
        $q$ et $u$ sont différentes, sans perte de généralité, supposons que
        $u>q$. Si les sauts ne sont possibles que vers la droite, c'est-à-dire
        $q=0$, on parle du processus totalement asymétrique (TASEP), sinon on
        parle du processus partiellement asymétrique (PASEP).
\end{itemize}
Dans la littérature combinatoire, le terme ASEP est généralement utilisé à la
place du terme SEP. De ce fait, dans le but de rester cohérent, nous utilisons
également cette terminologie.

Il existe de nombreuses variantes de ce modèle. On peut par exemple jouer sur le
nombre de types de particules ou encore sur la forme du réseau qui peut par
exemple être : $\ZZ$, $\ZZ/n\ZZ$, $\N$ ou $\llbracket 1,n \rrbracket$. Dans ce
manuscrit, nous étudions les modèles à un et deux types de particules sur le réseau
$\llbracket 1,n \rrbracket$. Nous parlons ainsi de l'\emph{ASEP à une particule}
et de l'\emph{ASEP à deux particules}. Si le nombre de particules n'est pas
mentionné il est alors question de l'ASEP à une
particule. En plus des paramètres $q$ et $u$, quatre autres paramètres
permettent de décrire le système (figure~\ref{fig:intro_asep}) : $\alpha$ et
$\gamma$ représentent la probabilité qu'une particule respectivement rentre et
sorte dans le réseau par la frontière gauche alors que $\delta$ et $\beta$
jouent respectivement le même rôle que $\alpha$ et $\gamma$ mais à la frontière
droite. Sans perte de généralité, autre que le cas dégénéré $q=u=0$, on peut
supposer $u=1$.
\begin{figure}[ht]
    \begin{center}
        \includegraphics[scale=.9]{images/asep_probabilites}
        \caption{Le processus d'exclusion simple asymétrique (ASEP).}
        \label{fig:intro_asep}
    \end{center}
\end{figure}

Les probabilités stationnaires des états de certains modèles de physique
statistique peuvent être calculées à partir d'un produit de matrices. L'outil
mathématique associé est l'\emph{\ansatz{} matriciel}. L'idée est d'associer à
chaque état un mot où les lettres correspondent à des matrices. Ces matrices
doivent vérifier des contraintes liées au système. Si on arrive à trouver une
représentation explicite de ces matrices, le calcul des probabilités
stationnaires se réduit à un produit matriciel. Par cette technique, d'autres
quantités physiques telles que la fonction de partition, le courant ou encore la
fonction de corrélation peuvent également s'exprimer en fonction d'un produit
matriciel. La fonction de partition est la quantité sur laquelle nous nous
focalisons dans cette thèse. La probabilité stationnaire d'un état est
généralement décrite à partir d'une mesure stationnaire qu'on renormalise. Ce
terme de renormalisation correspond justement à la \emph{fonction de partition},
c'est-à-dire à la somme des mesures stationnaires de tous les états. Plus
d'informations sur cet aspect physique du sujet ainsi que sur les techniques
liées à l'\ansatz{} matriciel peuvent être trouvées dans le \survey{} écrit par
Blythe et Evans \cite{BE}. L'ASEP à une et deux particules font partie des
modèles où on peut appliquer ces techniques. Dans le cas à une particule,
l'\ansatz{} matriciel que nous considérons est celui introduit par Derrida,
Evans, Hakim et Pasquier \cite{DEHP} et plus particulièrement la généralisation
plus flexible de Corteel et Williams \cite{CWte}. Dans le cas à deux particules,
nous utilisons la généralisation de Corteel, Mandelshtam et Williams \cite{CMW}
qui se base sur l'\ansatz{} matriciel introduit par Uchiyama \cite{U}. Dans les
deux cas, les contraintes que doivent vérifier les matrices permettent de
calculer récursivement les probabilités stationnaires sans avoir besoin de
représentation. Ce manuscrit se concentre sur cet aspect de l'\ansatz{}
matriciel.
\\

Les premières connexions entre la combinatoire et l'ASEP remontent, au
moins, à 1982 avec l'article de Shapiro et Zeilberger \cite{SZ}. Les auteurs
calculent la probabilité stationnaire d'un état dans le cas du TASEP avec
$\alpha=\beta=1$ et $\gamma=\delta=0$. L'aspect combinatoire de leur travail
est dû à la présence des nombres de Catalan et des partitions d'entiers. Duchi
et Schaeffer \cite{DS} donnent une explication combinatoire du lien avec les
nombres de Catalan en proposant une chaine de Markov sur les configurations
complètes, comptées par Catalan, qui se projette sur le TASEP avec
$\gamma=\delta=0$. Brak et Essam \cite{BE} quant à eux étudient ce même modèle à
l'aide de chemins pondérés. Depuis, les liens entre la combinatoire et l'ASEP
ont fait l'objet d'une recherche intensive. On peut notamment citer
\cite{BCEPR,CWtp1,CWtp2,V,CWte,Vansatz} pour l'ASEP à une particule et
\cite{M,MV,CMW} pour l'ASEP à deux particules. Les deux types de familles
d'objets qui sont le plus étudiés dans ce contexte sont les chemins pondérés et
les tableaux. C'est à ces derniers que nous nous intéressons dans cette thèse.

Idéalement, le but est de donner une interprétation combinatoire aux termes
apparaissant dans la fonction de partition, c'est-à-dire définir une
famille d'objets combinatoires pondérés de sorte que la série génératrice de
cette famille corresponde à la fonction de partition de l'ASEP. Il y a
principalement deux méthodes pour y arriver. Soit on montre que nos objets
vérifient l'\ansatz{} matriciel, soit on définit une chaîne de Markov sur nos
objets qui se projette sur l'ASEP. La deuxième solution est plus difficile mais
elle donne fondamentalement une meilleure interprétation. Les meilleurs
résultats concernant l'ASEP sont dûs à Dasse-Hartaut
\cite[Chapter~5]{DH} qui définit une chaine de Markov sur les tableaux
escaliers dans les cas $\delta=0$ et $q=1$. Quant à l'ASEP à deux particules,
Mandelshtam \cite[Section~3.4]{Mthesis} propose une chaine de Markov sur les
tableaux alternatifs rhombiques répondant ainsi au problème dans le cas
$\gamma=\delta=0$. Ces trois chaînes de Markov se basent sur le travail de
Corteel et Williams \cite{CWtp2} qui ont résolu le problème pour l'ASEP à une
particule et $\gamma=\delta=0$. En utilisant la première méthode, Corteel et
Williams \cite{CWte} puis Corteel, Mandelshtam et Williams \cite{CMW} répondent
respectivement aux problèmes à une et deux particules avec tous les paramètres
généraux.

Dans l'ASEP, certaines quantités peuvent être calculées à partir de la fonction
de partition, parmi lesquelles la valeur moyenne de la densité particulaire, sa
variance ou encore le courant. Ainsi, outre le fait que la fonction de partition
de l'ASEP peut être interprétée comme une série génératrice, en donner une
formule close est également intéressant d'un point de vue physique. Dans le cas
à une particule Corteel, Stanley, Stanton et Williams \cite{CSSW} en donnent une
formule close en ajoutant même un paramètre supplémentaire $y$ qui prend en
compte le nombre de particules d'un état. Toutefois, il n'est pas clair d'après
leur formule que la fonction de partition est un polynôme à coefficients
positifs. Si on pose $y=q=1$ on obtient une forme factorisée de la fonction de
partition \cite{BCEPR,USW,CWte,CDH}. Sinon, le meilleur résultat a été obtenu
par Josuat-Vergès \cite{JV} pour la spécialisation $\gamma=\delta=0$. Concernant
l'ASEP à deux particules, seul le cas $y=q=1$ a été résolu par Corteel,
Mandelshtam et Williams \cite{CMW}.
\\

Un autre aspect combinatoire de l'ASEP qu'il nous parait important de
développer, même si nous ne le développons pas en détail, est son lien avec
les polynômes orthogonaux. L'étude combinatoire de ces polynômes a débuté dans
les années 80 \cite{Foa,Fla,Vpolorth}. On a pu ainsi donner des interprétations
combinatoires des moments de nombreux polynômes orthogonaux appartenant au
schéma d'Askey \cite{KS}. La première apparition des polynômes
orthogonaux dans l'étude de l'ASEP est due à Sasamoto \cite{Sasa}. Celui-ci
étudie l'ASEP, dans le cas $\gamma=\delta=0$, à l'aide d'une interprétation
d'une représentation de l'\ansatz{} matriciel par les polynômes d'Al-Salam-Chihara
\cite{ASC}. Uchyama, Sasamoto et Wadati \cite{USW} généralisent le travail du
deuxième auteur en donnant une représentation de l'\ansatz{} matriciel avec tous
les paramètres généraux obtenue à partir des polynômes d'Askey-Wilson \cite{AW}.
Cela leur permet notamment de confirmer le diagramme de phase du courant obtenu
par Sandow \cite{Sand} à l'aide d'approximations. En généralisant
l'interprétation combinatoire de l'état stationnaire de l'ASEP, à tous les
paramètres, Corteel et Williams \cite{CWte} ont par la même occasion donné la
première interprétation combinatoire des moments des polynômes d'Askey-Wilson.
De plus, le calcul exact de ces moments \cite{CSSW} a permis d'obtenir une
formule close pour la fonction de partition. Lorsque l'on considère deux
particules, ce sont les polynômes de Koornwinder \cite{Koo} qui apparaissent
\cite{C,CWkoo}.

\subsection*{Implémentation informatique}
L'exploration assistée par ordinateur et la vérification informatique de nos
résultats ont composé une bonne partie de notre travail de recherche. Pour cela,
nous nous sommes servi du logiciel \textit{open-source} Sage \cite{sage} et de
l'extension développée par la communauté Sage-Combinat~\cite{sage-combinat}.
Nous avons notamment implémenté les classes combinatoires des tableaux
escaliers, des tableaux boisés et des polyominos parallélogrammes périodiques.
Actuellement ce code est expérimental et n'est pas réutilisable en l'état. À
terme, le but est d'intégrer ces objets à l'extension de sorte qu'il soit
disponible pour la communauté.

\section*{Contenu de la thèse}
\subsection*{Chapitre 1}
Ce chapitre introduit les notions combinatoires nécessaires à la compréhension
de cette thèse. Il débute par les séries génératrices en illustrant comment des
constructions combinatoires s'interprètent en équations sur les séries
génératrices. Suite à cela, nous définissons les différents objets combinatoires
classiques apparaissant dans cette thèse : les arbres, les chemins,
les diagrammes de Ferrers, les permutations, les polyominos parallélogrammes et
les cartes. Enfin, nous donnons une introduction sommaire aux chaines de Markov
notamment pour définir les probabilités stationnaires.
\subsection*{Chapitre 2}
Le deuxième chapitre est consacré à une nouvelle approche de l'interprétation
combinatoire par les tableaux escaliers de l'état stationnaire de l'ASEP.
Nous explicitons tout d'abord une récurrence que toute solution de
l'\ansatz{} matriciel doit satisfaire. Puis, en définissant un algorithme
d'insertion sur les tableaux escaliers, nous montrons qu'ils vérifient cette
même récurrence. Cet algorithme d'insertion donne une structure récursive simple
aux tableaux escaliers, de sorte que l'on retrouve les formes factorisées, déjà
connues, de spécialisations du polynôme générateur des tableaux escaliers. Une
autre conséquence de cet algorithme est une bijection entre les tableaux
escaliers et des tables d'inversions colorées qui nous permet de lire le poids
d'un tableau ainsi que ses cellules sud-est sur la table d'inversions image.
Cela donne une nouvelle construction pour définir des bijections entre les
tableaux escaliers et des permutations colorées. Nous retrouvons ainsi une
interprétation \cite{CDH} des
paramètres $\beta,\gamma$ et $q$ sur les permutations. Dans le cas
$\delta=\gamma=0$, nous conjecturons, sous la forme d'une équidistribution de
statistiques, une interprétation du bord sud-est ainsi que des paramètres
$\alpha,\beta$ et $q$ des tableaux escaliers sur les permutations.

Dans la suite du chapitre, nous proposons un algorithme d'insertion analogue
pour les tableaux escaliers de type B. De nouveau, la structure récursive simple
qui en découle donne une interprétation à des formes factorisées de
spécialisations du polynôme générateur. Nous obtenons des généralisations de
résultats précédents \cite{CDH} ainsi que de
nouveaux résultats. Enfin, nous définissons un \ansatz{} matriciel, avec tous les
paramètres généraux, compatible avec notre algorithme d'insertion.

Ce chapitre et le suivant correspondent à un travail de recherche récent dont
certaines pistes n'ont pas encore été complètement explorées. Il fera l'objet
d'un prochain article.

\subsection*{Chapitre 3}
Dans ce chapitre, nous étendons le premier algorithme d'insertion du chapitre
précédent au cas de l'ASEP à deux particules. Nous en déduisons que les tableaux
escaliers rhombiques vérifient une récurrence satisfaite par toute solution de
l'\ansatz{} matriciel. De plus, par cet algorithme d'insertion nous obtenons une
nouvelle bijection entre les assemblées de permutations et les tableaux
escaliers rhombiques n'ayant que des $\alpha$ et des $\beta$. Nous trouvons ainsi
une interprétation des paramètres $\alpha$ et $\beta$ analogue à \cite{MVass}. Dans le cas
où on remplace $\beta$ par $\delta$, on obtient également une interprétation du
paramètre $q$.

\subsection*{Chapitre 4}
Ce chapitre se place dans le cadre de l'ASEP à une particule dans le cas
$\gamma=\delta=0$. Il existe principalement trois familles de tableaux,
combinatoirement équivalentes, associées à ce cas particulier. Après quelques
définitions, le chapitre 4 débute par l'interprétation de l'énumération des
coins du bord sud-est de ces tableaux en termes d'ASEP. Suite à cela, nous
énumérons les coins occupés des tableaux escaliers, en prenant en compte les
paramètres $\alpha,\beta$ et $q$. Enfin, nous dénombrons les coins dans le
cas $\alpha=\beta=q=1$ et proposons un début de preuve pour $\alpha$ et $\beta$
généraux. Nous traitons également le type $B$ en parallèle.

L'essentiel de ce travail de recherche a fait l'objet des publications
\cite{LZ} et \cite{GGLZS}.

\subsection*{Chapitre 5}
Ce chapitre aborde un autre aspect de l'interprétation combinatoire de l'ASEP.
Les arbres non-ambigus introduits dans \cite{ABBS} correspondent aux structures
combinatoires arborescentes sous-jacente à l'ASEP. Nous proposons une définition
alternative de ces objets en tant qu'arbres binaires étiquetés. Cela nous permet de
leur appliquer une méthode développée dans \cite{HNT} et d'en déduire une
équation différentielle vérifiée par leur série génératrice. D'une part, on
montre ainsi la formule des équerres, déjà connue, comptant le nombre
d'arbres non-ambigus à arbre binaire fixé. D'autre part, la résolution de
l'équation différentielle nous permet d'obtenir une forme close de la série
génératrice ainsi que pour la formule d'énumération des arbres non-ambigus à
taille fixée. Le formalisme algébrique que nous utilisons nous permet de
raffiner facilement notre démarche en ajoutant des paramètres
supplémentaires à notre étude. Dans un premier temps, nous introduisons les
statistiques correspondant aux paramètres $\alpha$ et $\beta$ de l'ASEP. Nous
généralisons ainsi les formules closes obtenues précédemment. Dans un deuxième
temps, nous prenons en compte le nombre d'inversions ou l'indice majeur inverse
des permutations sous-jacentes aux arbres non-ambigus. Il en découle un
analogue polynomial de la formule des équerres ainsi qu'un raffinement de
l'équation différentielle faisant intervenir des $q$-analogues de l'intégrale
et de la dérivée.

La seconde partie de ce chapitre est consacré à une interprétation combinatoire
des résultats obtenus par le calcul. On y définit notamment une nouvelle
statistique sur les arbres binaires : le nombre d'équerres de l'unique
décomposition en équerres. De plus, nous relions notre interprétation
combinatoire au travail \cite{CE} effectué dans le cadre des permutations dont
toutes les excédences sont au début.

La plus grande partie du travail présenté dans ce chapitre ainsi que dans le
suivant a fait l'objet d'une publication \cite{ABDOHLZ} à la conférence
FPSAC, la version longue étant en cours de préparation.

\subsection*{Chapitre 6}

Les arbres non-ambigus correspondent à des objets de dimension $1$ plongés dans un
espace de dimension 2. Le sixième chapitre est consacré à la généralisation des arbres
non-ambigus en tant qu'objets de dimension $k$ plongés dans un espace de dimension
$d\geqslant k$. Ils sont tout d'abord définis comme des arbres
$\binom{k}{d}$-aires étiquetés. Ainsi, nous appliquons les techniques de
\cite{HNT} pour définir, à $k$ et $d$ fixés, une équation différentielle vérifiée
par la série génératrice de ces objets. Ce chapitre se termine par une
définition alternative géométrique, analogue à la première définition des arbres
non-ambigus.

\subsection*{Chapitre 7}

Ce chapitre est consacré à l'étude des polyominos parallélogrammes périodiques.
Il est indépendant des chapitres précédents. Nous étudions ces objets en
utilisant les méthodes de l'article \cite{BRS} qui donne un interprétation des
polyominos parallélogrammes et des polyominos convexes dirigés en termes
d'arbres ordonnés. Initialement, les polyominos parallélogrammes périodiques ont
été introduits dans le but de traiter le cas des polyominos convexes. Il est à
noter que Biagioli, Jouhet et Nadeau \cite{BJN} ont également introduit, de
façon indépendante, ces nouveaux objets dans le cadre des permutations affines
évitant $321$. En effet, à un marquage près, ils apparaissent naturellement
comme la généralisation des polyominos parallélogrammes qui sont en bijection
avec les permutations qui évitent $321$. En plus de ces nouveaux objets, nous
étudions également les polyominos parallélogrammes périodiques marqués,
introduits dans \cite{BJN}, et les bandes, qui sont les orbites sous l'action de
la rotation des colonnes des polyominos parallélogrammes périodiques.
L'adaptation de la construction de \cite{BRS} permet de calculer les séries
génératrices de ces trois classes combinatoires selon la hauteur, la largeur et
une nouvelle statistique appelée la hauteur de recollement intrinsèque. De même
que dans \cite{BRS}, l'aire est difficilement interprétable par cette méthode.
La hauteur de recollement intrinsèque permet de partitionner, pour chaque classe
combinatoire, l'ensemble infini des objets de demi-périmètre fixé en ensembles
finis de même taille. Dans le cas des bandes, nous obtenons une asymptotique de
cette taille. Pour les deux cas restants, nous donnons une formule d'énumération
à l'aide d'une interprétation mettant en jeu l'aire triangulaire sous les
chemins de Dyck. La dernière partie de ce chapitre est dédiée à l'étude de
l'ultime périodicité des séries génératrices selon l'aire, c'est-à-dire à
montrer que les coefficients sont périodiques à partir d'un certain rang. Cette
question est inspirée des groupes de Coxeter pour lesquels les séries
génératrices sont ultimement périodiques \cite{BJNult}. Nous présentons deux
constructions qui permettent d'étudier l'ultime périodicité de la série
génératrice de chacune des trois classes combinatoires. Elles consistent,
lorsque cela est possible, à retirer des cellules dans chaque colonne. Pour
chacune des deux, une sous-famille d'objets minimaux de chaque classe apparaît
naturellement : les maigres et les intrinsèquement minces. En étudiant ces deux
sous-famille, nous montrons que les polyominos parallélogrammes périodiques
maigres (resp. intrinsèquement minces) sont en bijection avec les chemins de
Dyck bilatères (resp. chemins de Dyck bilatères avec un pic marqué), ce qui nous
permet de les énumérer.

Ces travaux de recherche ont fait l'objet de l'article \cite{BLZ} et de la
prépublication \cite{ABLZP}.

\resumetocwriting
